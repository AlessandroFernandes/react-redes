import React, { Component } from 'react';
import history from './history';

export default class Login extends Component {
	constructor(props) {
		super(props);
		console.log(this.props);
		this.state = { msg: '' };
	}

	enviar(event) {
		event.preventDefault();

		const userPass = {
			method: 'POST',
			body: JSON.stringify({
				login: this.login.value,
				senha: this.senha.value,
			}),
			headers: new Headers({
				'Content-type': 'application/json',
			}),
		};
		fetch(`https://instalura-api.herokuapp.com/api/public/login`, userPass)
			.then(response => {
				if (response.ok) {
					return response.text();
				} else {
					throw new Error('Não foi possível logar!');
				}
			})
			.then(token => {
				localStorage.setItem('auth-token', token);
				history.push('/index');
			})
			.catch(erro => this.setState({ msg: `Usuário ou Senha Errada` }));
	}

	render() {
		return (
			<div className="login-box">
				<h1 className="header-logo">Instalura</h1>
				<form onSubmit={this.enviar.bind(this)}>
					{this.state.msg}
					<input type="text" ref={user => (this.login = user)} />
					<input type="password" ref={pass => (this.senha = pass)} />
					<input type="submit" value="login" />
				</form>
			</div>
		);
	}
}
