import React from 'react';
import ReactDOM from 'react-dom';
import './css/reset.css';
import './css/timeline.css';
import './css/login.css';
import Login from './componentes/login';
import { Router, Route, Redirect, Switch } from 'react-router-dom';
import history from './componentes/history';
import Timeline from './componentes/Timeline';

function PrivateRouter({ component: Component }) {
	return (
		<Route
			render={props =>
				localStorage.getItem('auth-token') ? (
					<Component {...props} />
				) : (
					<Redirect to="/" />
				)
			}
		/>
	);
}

ReactDOM.render(
	<Router history={history}>
		<Switch>
			<Route exact path="/" component={Login} />
			<PrivateRouter path="/index" component={Timeline} />
		</Switch>
	</Router>,
	document.getElementById('root'),
);
